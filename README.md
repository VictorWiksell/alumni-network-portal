# Alumni Network case 

## Background

The software's main goal is to serve as a culminating experience for the candidates. The applicants must create a piece of software that can be used as a finished product. Candidates must create and deploy a software program that enables direct messages—a form of group messaging—between groups, inside groups, and with specific individuals. 

## Installation

Requirements:

IntelliJ with Java 17, Spring Web, Spring Data JPA
Heroku
pgAdmin
Postgres
Figma
Postman
Keycloak
Swagger (optional)


## Frontend Requirements 

The login page, or a redirect to one, should be displayed to the user if they don't have an authorised user session. If they cannot authenticate, they shouldn't be able to move past the login screen.


An access token that can be used for further calls to the API endpoints should also result after authentication along with some user data (such as a profile image, name, etc.).

## User detail

There should be a profile view for each user in the system that is accessible to all other users. The following details need should be visible in the profile view:

• The user’s names
• A profile picture
• A work status message (i.e. “attending Experis Academy courses at Noroff”)
• A short bio or resume
• A fun fact about the user


## User profile Settings

Every user should have a settings view that corresponds to them and has a form to modify the fields listed in the User detail. Only the user's personal profile options are editable.


## User Dashboard

Displaying posts whose intended recipients are designated as the current user.


## Create or Edit Post

Users should be able to publish new content and update it for a specific audience or post answers to an event or post that has already occurred. A post ought to have the following details:

• A title 
• A body of the post

# Group List 

Display a list of public groups that other users have formed. Groups that the current user is a part of should be identified as such. The list may include private groups that the current user is a member of.

#Topic List 

Similar to the "group list," but with topics displayed in place of groups.

# Send Direct Message

A user can communicate directly with other users and get their responses.

## Contributors

@DanSho @VictorWiksell @Enwynn @nicolaspalaciosexperisgitlab 
