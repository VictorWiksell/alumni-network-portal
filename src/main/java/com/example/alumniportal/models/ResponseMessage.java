package com.example.alumniportal.models;

import lombok.Data;

@Data
public class ResponseMessage {
    private String message;
}

