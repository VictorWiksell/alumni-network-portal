package com.example.alumniportal.models;

/***
 * Represents a user, it's a main entity in this application.
 * Mapped by group_member to a ManyToMany relationship.
 * Mapped by topicMember to a ManyToMany relationship.
 * Mapped by senderUser to a oneToMany relationship
 * Mapped by targetUser to a oneToMany relationship
 */


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
public class AppUser {

    @Id
    private String uid;

    @Column(length = 50, nullable = false)
    private String name;


    @Column(length = 200)
    private String picture;

    @Column(length = 150)
    private String status;

    @Column(length = 300)
    private String bio;

    @Column(length = 300)
    private String funFact;

    @ManyToMany
    @JoinTable(name = "group_member",
            joinColumns = {@JoinColumn(name = "app_user_id")},
            inverseJoinColumns = {@JoinColumn(name = "app_group_id")})
    private Set<AppGroup> appGroup;

    @ManyToMany
    @JoinTable(name = "topicMember",
            joinColumns = {@JoinColumn(name = "app_user_uid")},
            inverseJoinColumns = {@JoinColumn(name = "topic_id ")})
    private Set<Topic> topics;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "senderUser", cascade = CascadeType.ALL)
    private Set<Post> sentPosts;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "targetUser", cascade = CascadeType.ALL)
    private Set<Post> receivedPosts;

    public void joinGroup(AppGroup group) {
        appGroup.add(group);
    }
}
