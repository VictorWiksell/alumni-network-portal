package com.example.alumniportal.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


/**
 * Represents a group, it's a main entity in this application.
 * Mapped to appGroup by a ManyToMany relationship.
 * Mapped to a targetGroup by a OneToMany relationship.
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
public class AppGroup {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Integer id;

    @Column(length = 50)
    private String fullName;

    @Column( length = 200)
    private String description;

    private boolean isPrivate;

    @ManyToMany(mappedBy = "appGroup")
    private Set<AppUser> appUser;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "targetGroup", cascade = CascadeType.ALL)
    private Set<Post> groupPosts;
}
