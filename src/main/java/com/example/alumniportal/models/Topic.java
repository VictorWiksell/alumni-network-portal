package com.example.alumniportal.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Represents a Topic, it's a main entity in this application
 * Mapped by topics by a ManyToMany relationship
 * Mapped by targetTopic by a OneToMany relationship
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Topic {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(name = "topic_id")
    private Integer id;

    @Column(length = 100)
    private String name;

    @Column(length = 500)
    private String description;

    @ManyToMany(mappedBy = "topics")
    private Set<AppUser> appUsers;

    @OneToMany(mappedBy = "targetTopic")
    private Set<Post> topicPosts;
}
