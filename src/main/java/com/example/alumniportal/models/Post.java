package com.example.alumniportal.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Represents a Post, it's a main entity in this application
 * Mapped to sentPost_id by a ManyToOne relationship
 * Mapped to receivedPosts_id by a ManyToOne relationship
 * Mapped to topicPosts_id by a ManyToOne relationship
 * Mapped to groupPosts_id by a ManyToOne relationship
 */

@Getter
@Setter
@ToString
@Entity
public class Post {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    @Column(name = "post_id")
    private Integer id;

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", shape = JsonFormat.Shape.STRING)
    private LocalDateTime createDateTime;

    private Integer answeredTo;


    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "sentPosts_id")
    private AppUser senderUser;

    @Column(length = 500)
    private String postMessage;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "receivedPosts_id")
    private AppUser targetUser;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "topicPosts_id")
    private Topic targetTopic;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "groupPosts_id")
    private AppGroup targetGroup;
}
