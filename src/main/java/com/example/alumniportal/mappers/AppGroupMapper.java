package com.example.alumniportal.mappers;

import com.example.alumniportal.dto.AppGroupDTO;
import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.services.AppUser.AppUserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class AppGroupMapper {

    @Autowired
    protected AppUserService appUserService;

    /**
     * Method for converting a appGroup to appGroupDTO
     *
     * @param appGroup takes in a appGroup object
     * @return appUsers to ids
     */
    @Mapping(target = "appUser", source = "appUser", qualifiedByName = "appUsersToIds")
    public abstract AppGroupDTO appGroupToAppGroupDTO(AppGroup appGroup);

    public abstract Collection<AppGroupDTO> appGroupToAppGroupDTO(Collection<AppGroup> appGroup);

    /**
     * Method for converting appGroupDTO to appGroup
     *
     * @param appGroupDTO ids to appGroupDTO
     * @return userIds to appUser
     */
    @Mapping(target = "appUser", source = "appUser", qualifiedByName = "userIdsToAppUser")
    public abstract AppGroup appGroupDTOToAppGroup(AppGroupDTO appGroupDTO);

    @Named("userIdsToAppUser")
    Set<AppUser> userIdsToAppUsers(Set<String> uid) {
        return uid.stream()
                .map(i -> appUserService.findById(i))
                .collect(Collectors.toSet());

    }

    @Named("appUsersToIds")
    Set<String> mapAppUsersToIds(Set<AppUser> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(AppUser::getUid).collect(Collectors.toSet());
    }
}


