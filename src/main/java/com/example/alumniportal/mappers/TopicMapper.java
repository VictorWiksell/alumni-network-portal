package com.example.alumniportal.mappers;

import com.example.alumniportal.dto.TopicDTO;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Post;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.services.AppUser.AppUserService;
import com.example.alumniportal.services.Post.PostService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper class for Topics
 */
@Mapper(componentModel = "spring")
public abstract class TopicMapper {
    @Autowired
    protected AppUserService appUserService;

    @Autowired
    protected PostService postService;

    /**
     * Method for converting topic to topicDTO
     *
     * @param topic takes in a topic object
     * @return topic and appUser to ids
     */
    @Mapping(target = "topicPosts", source = "topicPosts", qualifiedByName = "topicPostsToIds")
    @Mapping(target = "appUsers", source = "appUsers", qualifiedByName = "appUsersToIds")
    public abstract TopicDTO topicToTopicDTO(Topic topic);

    public abstract Collection<TopicDTO> topicToTopicDTO(Collection<Topic> topics);

    /**
     * Method for converting topicDTO to topic
     *
     * @param topicDTO takes in a topicDTO object
     * @return topic and appUser ids to topic and appUser
     */
    @Mapping(target = "topicPosts", source = "topicPosts", qualifiedByName = "topicIdsToTopicPosts")
    @Mapping(target = "appUsers", source = "appUsers", qualifiedByName = "appUsersIdsToAppUsers")
    public abstract Topic topicDTOToTopic(TopicDTO topicDTO);


    @Named("appUsersIdsToAppUsers")
    Set<AppUser> mapIdsToAppUsers(Set<String> uid) {
        return uid.stream()
                .map(i -> appUserService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("appUsersToIds")
    Set<String> mapAppUsersToIds(Set<AppUser> appUsers) {
        if (appUsers == null || appUsers.isEmpty()) {
            return null;
        }
        return appUsers.stream()
                .map(AppUser::getUid).collect(Collectors.toSet());
    }

    @Named("topicIdsToTopicPosts")
    Set<Post> mapIdsToTopicPosts(Set<Integer> id) {
        return id.stream()
                .map(i -> postService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("topicPostsToIds")
    Set<Integer> mapTopicPostsToIds(Set<Post> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
