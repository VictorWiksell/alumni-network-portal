package com.example.alumniportal.mappers;

import com.example.alumniportal.dto.AppUserDTO;
import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Post;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.services.AppGroup.AppGroupService;
import com.example.alumniportal.services.Post.PostService;
import com.example.alumniportal.services.topic.TopicService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A mapper class for appUsers, post and Topics.
 */
@Mapper(componentModel = "spring")
public abstract class AppUserMapper {

    @Autowired
    protected AppGroupService appGroupService;

    @Autowired
    protected PostService postService;

    @Autowired
    protected TopicService topicService;

    /***
     * Method for converting a appUser to a DTO
     *
     * @param appUser takes in a appUser object
     *
     * @return appGroups, topic and post ids
     */
    @Mapping(target = "appGroup", source = "appGroup", qualifiedByName = "appGroupsToIds")
    @Mapping(target = "topics", source = "topics", qualifiedByName = "topicsToIds")
    @Mapping(target = "sentPosts", source = "sentPosts", qualifiedByName = "sentPostsToIds")
    @Mapping(target = "receivedPosts", source = "receivedPosts", qualifiedByName = "receivedPostsToIds")
    public abstract AppUserDTO appUserToAppUserDTO(AppUser appUser);

    public abstract Collection<AppUserDTO> appUserToAppUserDTO(Collection<AppUser> appuser);

    /***
     * Method for converting a appUserDTO to a appUser
     *
     * @param appUserDTO takes in a appUserDTO object
     *
     * @return groups, topic and post ids to groups,topic and post
     */
    @Mapping(target = "appGroup", source = "appGroup", qualifiedByName = "groupsIdsToGroups")
    @Mapping(target = "topics", source = "topics", qualifiedByName = "topicsIdsToTopics")
    @Mapping(target = "sentPosts", source = "sentPosts", qualifiedByName = "sentPostIdsToSentPosts")
    @Mapping(target = "receivedPosts", source = "receivedPosts", qualifiedByName = "receivedPostsIdsToReceivedPosts")
    public abstract AppUser appUserDTOToAppUser(AppUserDTO appUserDTO);

    @Named("groupsIdsToGroups")
    Set<AppGroup> mapIdsToAppGroup(Set<Integer> id) {
        return id.stream()
                .map(i -> appGroupService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("appGroupsToIds")
    Set<Integer> mapAppGroupsToIds(Set<AppGroup> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());

    }

    @Named("topicsIdsToTopics")
    Set<Topic> mapIdsToTopic(Set<Integer> id) {
        return id.stream()
                .map(i -> topicService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("topicsToIds")
    Set<Integer> mapTopicsToIds(Set<Topic> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());


    }

    @Named("sentPostIdsToSentPosts")
    Set<Post> mapSentPostIdsToSentPost(Set<Integer> id) {
        return id.stream()
                .map(i -> postService.findById(i))
                .collect(Collectors.toSet());

    }

    @Named("sentPostsToIds")
    Set<Integer> mapSentPostsToIds(Set<Post> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(p -> p.getId()).collect(Collectors.toSet());
    }

    @Named("receivedPostsIdsToReceivedPosts")
    Set<Post> mapReceivedPostIdsToReceivedPosts(Set<Integer> id) {
        return id.stream()
                .map(i -> postService.findById(i))
                .collect(Collectors.toSet());

    }

    @Named("receivedPostsToIds")
    Set<Integer> mapReceivedPostsToIds(Set<Post> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(p -> p.getId()).collect(Collectors.toSet());
    }
}


