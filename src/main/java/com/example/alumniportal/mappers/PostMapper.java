package com.example.alumniportal.mappers;

import com.example.alumniportal.dto.PostDTO;
import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Post;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.services.AppGroup.AppGroupService;
import com.example.alumniportal.services.AppUser.AppUserService;
import com.example.alumniportal.services.topic.TopicService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;


/**
 * A mapper class fot Posts, appGroup, appUser and topic
 */
@Mapper(componentModel = "spring")
public abstract class PostMapper {

    @Autowired
    protected AppGroupService appGroupService;

    @Autowired
    protected AppUserService appUserService;

    @Autowired
    protected TopicService topicService;

    /**
     * Method for converting a post to a postDTO
     *
     * @param post takes in a post object
     * @return appUser, appGroup ids
     */
    @Mapping(target = "name", source = "senderUser.uid", qualifiedByName = "userNameToDTO")
    @Mapping(target = "picture", source = "senderUser.uid", qualifiedByName = "userPictureToDTO")
    @Mapping(target = "targetGroup", source = "targetGroup.id")
    @Mapping(target = "targetTopic", source = "targetTopic.id")
    @Mapping(target = "senderUser", source = "senderUser.uid")
    @Mapping(target = "targetUser", source = "targetUser.uid")
    public abstract PostDTO PostToPostDTO(Post post);

    public abstract Collection<PostDTO> postToPostDTO(Collection<Post> posts);

    /**
     * Method for converting postDTO to post
     *
     * @param postDTO as ids
     * @return appGroup, topic and appUser ids to appGroup, topic and appUser
     */
    @Mapping(target = "targetGroup", source = "targetGroup", qualifiedByName = "groupsIdsToTargetGroup")
    @Mapping(target = "targetTopic", source = "targetTopic", qualifiedByName = "topicIdsToTargetTopic")
    @Mapping(target = "senderUser", source = "senderUser", qualifiedByName = "userIdsToSenderUser")
    @Mapping(target = "targetUser", source = "targetUser", qualifiedByName = "userIdsToTargetUser")
    public abstract Post postDtoToPost(PostDTO postDTO);

    @Named("groupsIdsToTargetGroup")
    AppGroup appGroupIdToDTO(int id) {
        return appGroupService.findById(id);
    }

    @Named("userPictureToDTO")
    String pictureToDTO (String id) {
        return appUserService.findById(id).getPicture();
    }


    @Named("topicIdsToTargetTopic")
    Topic appTopicIdToDTO(int id) {
        return topicService.findById(id);
    }

    @Named("userIdsToSenderUser")
    AppUser senderUserIdToDTO(String id) {
        return appUserService.findById(id);
    }

    @Named("userIdsToTargetUser")
    AppUser targetUserIdToDTO(String id) {
        return appUserService.findById(id);
    }

    @Named("userNameToDTO")
    String userNameToDTO(String id) {
        return appUserService.findById(id).getName();
    }
}
