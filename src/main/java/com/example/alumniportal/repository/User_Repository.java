package com.example.alumniportal.repository;

import com.example.alumniportal.models.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface User_Repository extends JpaRepository<AppUser, String> {
}
