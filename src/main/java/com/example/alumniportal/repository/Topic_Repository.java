package com.example.alumniportal.repository;

import com.example.alumniportal.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Topic_Repository extends JpaRepository<Topic, Integer> {
}
