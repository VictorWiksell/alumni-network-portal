package com.example.alumniportal.repository;

import com.example.alumniportal.models.AppGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Group_Repository extends JpaRepository<AppGroup, Integer> {
}
