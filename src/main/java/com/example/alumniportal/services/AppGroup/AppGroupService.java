package com.example.alumniportal.services.AppGroup;

import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.services.CrudServices;

public interface AppGroupService extends CrudServices<AppGroup, Integer> {
    /**
     * Custom method adds a appGroup to a appUser
     *
     * @param takes given uid and app group
     */
    AppGroup addGroup(String uid, AppGroup appGroup);
}
