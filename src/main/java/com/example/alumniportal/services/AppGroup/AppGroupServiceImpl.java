package com.example.alumniportal.services.AppGroup;

import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.repository.Group_Repository;
import com.example.alumniportal.repository.User_Repository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class AppGroupServiceImpl implements AppGroupService {

    private final Group_Repository groupRepository;

    private final User_Repository userRepository;

    public AppGroupServiceImpl(Group_Repository groupRepository, User_Repository userRepository) {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    /**
     * Method to find a appGroup by id
     *
     * @param id takes in appGroup id
     * @return a single appGroup
     */
    @Override
    public AppGroup findById(Integer id) {
        return groupRepository.findById(id).orElse(null);
    }

    /**
     * Method to find all appGroup
     *
     * @return a collection of appGroup
     */
    @Override
    public Collection<AppGroup> findAll() {
        return groupRepository.findAll();
    }

    /**
     * Method to add a appGroup
     *
     * @param appGroup takes a appGroup entity
     * @return saves the appGroup entity
     */
    @Override
    public AppGroup add(AppGroup appGroup) {
        return groupRepository.save(appGroup);
    }

    /**
     * Method to add a appGroup to a user
     *
     * @param appGroup takes a appGroup entity and user id as a string
     * @return saves the appUser entity and adds a group to the appUser entity
     */
    @Override
    public AppGroup addGroup(String uid, AppGroup appGroup) {
        AppUser user = userRepository.findById(uid).get();
        Set<AppGroup> appGroups = user.getAppGroup();
        appGroups.add(appGroup);
        user.setAppGroup(appGroups);
        return groupRepository.save(appGroup);
    }

    /**
     * Method to update a appGroup
     *
     * @param entity takes a group entity
     * @return saves the appGroup
     */
    @Override
    public AppGroup update(AppGroup entity) {
        return null;
    }

    /**
     * Method for deleting a appGroup by id
     *
     * @param integer takes given id
     */
    @Override
    public void deleteById(Integer integer) {

    }

    /**
     * Custom method searches if an id exists in the DB
     *
     * @param id the id your looking for
     */
    @Override
    public boolean exists(Integer integer) {
        return false;
    }
}
