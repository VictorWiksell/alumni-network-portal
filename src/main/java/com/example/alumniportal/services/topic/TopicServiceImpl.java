package com.example.alumniportal.services.topic;

import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.repository.Topic_Repository;
import com.example.alumniportal.repository.User_Repository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class TopicServiceImpl implements TopicService {
    private final Topic_Repository topicRepository;
    private final User_Repository userRepository;
    private final User_Repository appUser;

    public TopicServiceImpl(Topic_Repository topicRepository, User_Repository userRepository, User_Repository appUser) {
        this.topicRepository = topicRepository;
        this.userRepository = userRepository;
        this.appUser = appUser;
    }

    /**
     * Method to find a topic by id
     *
     * @param id takes in topic id
     * @return a single topic
     */
    @Override
    public Topic findById(Integer id) {
        return topicRepository.findById(id).orElse(null);
    }

    /**
     * Method to find all topics
     *
     * @return a collection of topics
     */
    @Override
    public Collection<Topic> findAll() {
        return topicRepository.findAll();
    }

    /**
     * Method to add a topic
     *
     * @param entity takes a topic entity
     * @return saves the topic entity
     */
    @Override
    public Topic add(Topic entity) {
        return null;
    }

    /**
     * Method to add a topic to a user
     *
     * @param uid and topic takes a appGroup entity and user id as a string
     * @return saves the appUser entity and adds a group to the appUser entity
     */
    @Override
    public Topic addTopic(String uid, Topic topic) {
        AppUser user = userRepository.findById(uid).get();
        Topic topic1 = topicRepository.save(topic);
        Set<Topic> topics = user.getTopics();
        topics.add(topic1);
        user.setTopics(topics);
        userRepository.save(user);
        return topicRepository.save(topic);
    }

    /**
     * Method to update a topic
     *
     * @param entity takes a topic entity
     * @return saves the topic
     */
    @Override
    public Topic update(Topic entity) {
        return topicRepository.save(entity);
    }

    /**
     * Method for deleting a topic by id
     *
     * @param integer takes given id
     */
    @Override
    public void deleteById(Integer integer) {

    }

    /**
     * Custom method searches if an id exists in the DB
     *
     * @param id the id your looking for
     */
    @Override
    public boolean exists(Integer integer) {
        return false;
    }

    /**
     * Method that allows a user to follow topic
     * @param  userId and topicId takes given ids*/
    @Override
    public Topic followTopic(String userId, int topicId) {
        Topic topic = topicRepository.findById(topicId).orElse(null);
        AppUser user = userRepository.findById(userId).get();
        user.getTopics().add(topic);

        userRepository.save(user);
        return topicRepository.save(topic);
    }
}
