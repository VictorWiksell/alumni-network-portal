package com.example.alumniportal.services.topic;

import com.example.alumniportal.models.Topic;
import com.example.alumniportal.services.CrudServices;

public interface TopicService extends CrudServices<Topic, Integer> {

    /**
     * Custom method allows user to follow a topic
     *
     * @param userId and yopicId takes given ids
     */
    Topic followTopic(String userId, int topicId);

    /**
     * Custom method adds a topic to a appUser
     *
     * @param takes given uid and a topic entity
     */
    Topic addTopic(String uid, Topic topic);
}
