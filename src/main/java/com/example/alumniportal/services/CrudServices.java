package com.example.alumniportal.services;

import java.util.Collection;

public interface CrudServices<T, ID> {
    T findById(ID id);

    Collection<T> findAll();

    T add(T entity);

    T update(T entity);

    void deleteById(ID id);

    boolean exists(ID id);
}
