package com.example.alumniportal.services.AppUser;

import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.repository.Group_Repository;
import com.example.alumniportal.repository.User_Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AppUserServiceImpl implements AppUserService {

    private final User_Repository userRepository;
    private final Group_Repository groupRepository;

    public AppUserServiceImpl(User_Repository userRepository, Group_Repository groupRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
    }

    /**
     * Method to find a appUser by id
     *
     * @param uid takes in appUser id
     * @return a single appUser else null
     */
    @Override
    public AppUser findById(String uid) {
        return userRepository.findById(uid).orElse(null);
    }

    /**
     * Method to find all appUsers
     *
     * @return a list of appUsers
     */
    @Override
    public List<AppUser> findAll() {
        return userRepository.findAll();
    }

    /**
     * Method to add a post to a appUser
     *
     * @param entity takes a appUser entity
     * @return saves the movie appUser entity
     */
    @Override
    public AppUser add(AppUser entity) {
        return null;
    }

    /**
     * Method to register a appUser
     *
     * @param uid and name takes Strings as given parameters
     * @return saves the appUser
     */
    @Override
    public AppUser addUser(String uid, String name) {
        AppUser user = new AppUser();
        user.setUid(uid);
        user.setName(name);
        return userRepository.save(user);
    }

    /**
     * Method to update a appUser
     *
     * @param appUser and uid takes String and appUser entity as given parameters
     * @return saves the updated appUser
     */
    @Override
    public AppUser setInfo(AppUser appUser, String uid) {
        AppUser user = userRepository.findById(uid).orElse(null);
        user.setName(appUser.getName());
        user.setPicture(appUser.getPicture());
        user.setStatus(appUser.getStatus());
        user.setBio(appUser.getBio());
        user.setFunFact(appUser.getFunFact());
        Set<Topic> topicList = appUser.getTopics();

        return userRepository.save(user);
    }

    /**
     * Method add a post to specific user
     *
     * @param appUser appUser entity as given parameters
     * @return saves the updated appUser
     */
    @Override
    public AppUser update(AppUser appUser) {
        AppUser user = userRepository.findById(appUser.getUid()).orElseThrow();
        user.setName(appUser.getName());
        user.setBio(appUser.getBio());
        user.setFunFact(appUser.getFunFact());
        user.setPicture(appUser.getPicture());
        user.setStatus(appUser.getStatus());
        return userRepository.save(user);
    }

    /**
     * Method for deleting a post by id
     *
     * @param s takes given app user id
     */
    @Override
    public void deleteById(String s) {

    }

    /**
     * Method for joining a group
     *
     * @param userId and groupId takes given userId and appGroupId
     */
    @Override
    public AppUser joinGroup(String userId, int groupId) {
        Optional<AppUser> user = userRepository.findById(userId);
        Optional<AppGroup> group = groupRepository.findById(groupId);
        AppGroup newGroup = group.get();
        AppUser newUser = user.get();
        newUser.getAppGroup().add(newGroup);


        return userRepository.save(newUser);
    }


    /**
     * Custom method searches if an id exists in the DB
     *
     * @param id the id your looking for
     */
    @Override
    public boolean exists(String s) {
        return userRepository.existsById(s);
    }
}