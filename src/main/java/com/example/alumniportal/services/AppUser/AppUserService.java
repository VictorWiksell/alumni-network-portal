package com.example.alumniportal.services.AppUser;

import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.services.CrudServices;

public interface AppUserService extends CrudServices<AppUser, String> {
    /**
     * Custom method registers a user by jwt token
     *
     * @param uid and name takes given name and user id
     */
    AppUser addUser(String uid, String name);

    /**
     * Custom method for updating a users infromation
     *
     * @param uid and name takes given name and user id
     */
    AppUser setInfo(AppUser appUser, String uid);

    /**
     * Custom method for appUser to join a group
     *
     * @param userId and groupId takes given userId and groupId
     */
    AppUser joinGroup(String userId, int groupId);

}
