package com.example.alumniportal.services.Post;

import com.example.alumniportal.models.Post;
import com.example.alumniportal.services.CrudServices;

import java.util.Collection;

public interface PostService extends CrudServices<Post, Integer> {

    /**
     * Custom method finds posts sent to user
     *
     * @param id and targetUid takes given name and user id
     */
    Collection<Post> findSentPosts(String id, String targetUid);

    /**
     * Custom method finds posts received to user
     *
     * @param id given postid
     */
    Collection<Post> findReceivedPosts(String id);

    /**
     * Custom method finds target groups posts
     *
     * @param groupId given postid
     */
    Collection<Post> findGroupPosts(Integer groupId);

    /**
     * Custom method finds target topic posts
     *
     * @param id given postid
     */
    Collection<Post> findTopicPosts(Integer id);

    /**
     * Custom method adds a post to target group
     *
     * @param uid, groupId and post given userId, groupId and post entity
     */
    Post addGroupPost(String uid, Integer groupId, Post post);

    /**
     * Custom method adds a post to target topic
     *
     * @param uid, topicId and post given userId, topicId and post entity
     */
    Post addTopicPost(String uid, Integer topicId, Post post);

    /**
     * Custom method answers to a target group post
     *
     * @param uid, groupId and postId, targetUid and post given userId, groupId, targetUser and post entity
     */
    Post answerToGroupPosts(String uid, Integer groupId, Integer postId, String targetUid, Post post);

    /**
     * Custom method answers to a target topic post
     *
     * @param uid, topicId and postId, targetUid and post given userId, groupId, targetUser and post entity
     */
    Post answerToTopicPost(String uid, Integer topicId, Integer postId, String targetUid, Post post);

    /**
     * Custom method for communicating between users
     *
     * @param uid, targetUid and post, given userId and targetUid, post entity
     */
    Post sendDM(String uid, String targetUid, Post post);
}
