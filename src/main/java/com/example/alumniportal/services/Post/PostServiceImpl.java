package com.example.alumniportal.services.Post;

import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Post;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.repository.Group_Repository;
import com.example.alumniportal.repository.PostRepository;
import com.example.alumniportal.repository.Topic_Repository;
import com.example.alumniportal.repository.User_Repository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;


@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final User_Repository appUser;

    private final Topic_Repository topicRepository;

    private final Group_Repository groupRepo;

    public PostServiceImpl(PostRepository postRepository, User_Repository appUser, Topic_Repository topic, Topic_Repository topicRepository, Group_Repository groupRepo) {
        this.postRepository = postRepository;
        this.appUser = appUser;
        this.topicRepository = topicRepository;
        this.groupRepo = groupRepo;
    }

    /**
     * Method to add a post to a target group
     *
     * @param uid, groupId and post takes a groupId, userId, and post entity
     * @return saves the post to a group
     */
    @Override
    public Post addGroupPost(String uid, Integer groupId, Post post) {
        AppGroup group = groupRepo.findById(groupId).get();
        AppUser user = appUser.findById(uid).get();
        post.setTargetGroup(group);
        post.setSenderUser(user);
        group.getGroupPosts().add(post);
        user.getSentPosts().add(post);
        post.setAnsweredTo(0);

        return postRepository.save(post);
    }

    /**
     * Method to add a post to a target topic
     *
     * @param uid, topicId and post takes a topicId, userId, and post entity
     * @return saves the post to a topic
     */
    @Override
    public Post addTopicPost(String uid, Integer topicId, Post post) {
        Topic topic = topicRepository.findById(topicId).get();
        AppUser user = appUser.findById(uid).get();
        post.setTargetTopic(topic);
        post.setSenderUser(user);
        post.setAnsweredTo(0);

        return postRepository.save(post);
    }

    /**
     * Method to answer to a post in a group
     *
     * @param uid, groupId and postId, targetUid and post takes a uid, groupId, postId  and post entity
     * @return saves the post to a post
     */
    @Override
    public Post answerToGroupPosts(String uid, Integer groupId, Integer postId, String targetUid, Post post) {
        AppGroup group = groupRepo.findById(groupId).get();
        AppUser user = appUser.findById(uid).get();
        AppUser targetUser = appUser.findById(targetUid).get();

        post.setTargetGroup(group);
        post.setSenderUser(user);
        post.setAnsweredTo(postId);
        post.setTargetUser(targetUser);

        group.getGroupPosts().add(post);
        user.getSentPosts().add(post);
        targetUser.getReceivedPosts().add(post);
        return postRepository.save(post);
    }

    /**
     * Method to answer to a post in a topic
     *
     * @param uid, topicId and postId, targetUid and post takes a uid, groupId, postId  and post entity
     * @return saves the post to a post
     */
    @Override
    public Post answerToTopicPost(String uid, Integer topicId, Integer postId, String targetUid, Post post) {
        Topic topic = topicRepository.findById(topicId).get();
        AppUser user = appUser.findById(uid).get();
        AppUser targetUser = appUser.findById(targetUid).get();

        post.setTargetTopic(topic);
        post.setSenderUser(user);
        post.setAnsweredTo(postId);
        post.setTargetUser(targetUser);

        topic.getTopicPosts().add(post);
        user.getSentPosts().add(post);
        targetUser.getReceivedPosts().add(post);
        return postRepository.save(post);
    }

    /***
     * Method that finds a received post to a target user
     * @param id takes a user id as a string
     * @return gets a targetUsers post
     */
    @Override
    public Collection<Post> findReceivedPosts(String id) {
        return appUser.findById(id).get().getReceivedPosts();
    }

    /***
     * Method that finds a sent post from a target user
     * @param id, targetUid takes a user id and targetUid as a string
     * @return gets a appUser sender post
     */
    @Override
    public Collection<Post> findSentPosts(String id, String targetUid) {
        Set<Post> post = appUser.findById(id).get().getSentPosts();
        post.removeIf(p -> !p.getTargetUser().getUid().equals(targetUid));

        Set<Post> post1 = appUser.findById(targetUid).get().getSentPosts();
        post1.removeIf(p -> !p.getTargetUser().getUid().equals(id));

        post.addAll(post1);
        return post;
    }

    /**
     * Method that finds a target groups post
     *
     * @param groupId takes a groups id
     * @return gets a target groups posts
     */
    @Override
    public Collection<Post> findGroupPosts(Integer groupId) {
        return groupRepo.findById(groupId).get().getGroupPosts();
    }

    /**
     * Method that finds a target topic post
     *
     * @param id takes a topic id
     * @return gets a target topic posts
     */
    public Collection<Post> findTopicPosts(Integer id) {
        return topicRepository.findById(id).get().getTopicPosts();
    }

    /**
     * Method that finds a post by id
     *
     * @param integer takes a post id
     * @return finds a specific post by id
     */
    @Override
    public Post findById(Integer integer) {
        return postRepository.findById(integer).get();
    }

    /**
     * Method that finds all post
     *
     * @return a list of posts
     */
    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    /**
     * Method that allows users to communicate
     *
     * @return saves a message to a user
     */
    @Override
    public Post sendDM(String uid, String targetUid, Post post) {
        AppUser user = appUser.findById(uid).get();
        AppUser targetUser = appUser.findById(targetUid).get();
        post.setSenderUser(user);
        post.setTargetUser(targetUser);
        user.getSentPosts().add(post);
        targetUser.getReceivedPosts().add(post);

        return postRepository.save(post);

    }

    /**
     * Method that adds a post
     *
     * @return saves a post
     */
    @Override
    public Post add(Post entity) {
        return postRepository.save(entity);
    }

    /**
     * Method that updates a post
     *
     * @return saves the updated post
     */
    @Override
    public Post update(Post entity) {
        return postRepository.save(entity);
    }

    /**
     * Method that deletes a post by id
     *
     */
    @Override
    public void deleteById(Integer integer) {
        Post post = postRepository.findById(integer).get();
    }

    /**
     * Custom method searches if an id exists in the DB
     *
     * @param id the id your looking for
     */
    @Override
    public boolean exists(Integer integer) {
        return false;
    }
}
