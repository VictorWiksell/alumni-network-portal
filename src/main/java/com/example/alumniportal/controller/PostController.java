package com.example.alumniportal.controller;

import com.example.alumniportal.dto.AppGroupDTO;
import com.example.alumniportal.dto.PostDTO;
import com.example.alumniportal.mappers.PostMapper;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.models.Post;
import com.example.alumniportal.services.Post.PostServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@CrossOrigin(origins = "*")
@AllArgsConstructor
@RequestMapping("api/v1/post")
public class PostController {
    private final PostServiceImpl postService;
    private final PostMapper postMapper;

    @Operation(summary = "Returns all posts")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = PostDTO.class)))})
    })
    @GetMapping("/get")
    public ResponseEntity getAllPosts() {
        Collection<PostDTO> posts = postMapper.postToPostDTO(postService.findAll());
        return ResponseEntity.ok(posts);
    }

    @Operation(summary = "Returns posts sent to target user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDTO.class))}),
    })
    @GetMapping("/conversation/{targetUid}")
    public ResponseEntity findSentPostsToUser(@AuthenticationPrincipal Jwt jwt, @PathVariable("targetId") String targetUid) {
        Collection<PostDTO> posts = postMapper.postToPostDTO(postService.findSentPosts(jwt.getClaimAsString("sub"), targetUid));
        return ResponseEntity.ok(posts);
    }

    @Operation(summary = "Returns posts received to target user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDTO.class))}),
    })
    @GetMapping("/user")
    public ResponseEntity findReceivedPosts(@AuthenticationPrincipal Jwt jwt) {
        Collection<PostDTO> posts = postMapper.postToPostDTO(postService.findReceivedPosts(jwt.getClaimAsString("sub")));
        return ResponseEntity.ok(posts);
    }

    @Operation(summary = "Returns posts received for a group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppGroupDTO.class))}),
    })
    @GetMapping("/group/{groupId}")
    public ResponseEntity findGroupPosts(@PathVariable("groupId") Integer groupId) {
        Collection<PostDTO> posts = postMapper.postToPostDTO(postService.findGroupPosts(groupId));
        return ResponseEntity.ok(posts);
    }

    @Operation(summary = "Returns posts received for a topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDTO.class))}),
    })
    @GetMapping("/topic/{topicId}")
    public ResponseEntity findTopicPosts(@PathVariable("topicId") Integer topicId) {
        Collection<PostDTO> posts = postMapper.postToPostDTO(postService.findTopicPosts(topicId));
        return ResponseEntity.ok(posts);
    }

    @Operation(summary = "Creates a new post")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Post successfully created",
                    content = @Content)
    })
    @PostMapping("/add")
    public ResponseEntity add(@RequestBody Post post) {

        return ResponseEntity.ok(postService.add(post));
    }

    @Operation(summary = "Creates a new post in a target group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Post successfully created",
                    content = @Content)
    })
    @PostMapping("/grouppost/{groupId}")
    public ResponseEntity addGroupPost(@AuthenticationPrincipal Jwt jwt, @PathVariable("groupId") int groupId, @RequestBody Post post) {

        return ResponseEntity.ok(postService.addGroupPost(jwt.getClaimAsString("sub"), groupId, post));
    }

    @Operation(summary = "Created a new post in a target topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Post successfully created",
                    content = @Content)
    })
    @PostMapping("/topicpost/{topicId}")
    public ResponseEntity addTopicPost(@AuthenticationPrincipal Jwt jwt, @PathVariable("topicId") int topicId, @RequestBody Post post) {

        return ResponseEntity.ok(postService.addTopicPost(jwt.getClaimAsString("sub"), topicId, post));
    }

    @Operation(summary = "Created a new post in a group post")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Post successfully created",
                    content = @Content)
    })
    @PostMapping("/group/{groupId}/post/{postId}/user/{targetUid}")
    public ResponseEntity answerToGroupPosts(@AuthenticationPrincipal Jwt jwt, @PathVariable("groupId") int groupId, @PathVariable("postId") int postId, @PathVariable("targetUid") String targetUid, @RequestBody Post post) {

        return ResponseEntity.ok(postService.answerToGroupPosts(jwt.getClaimAsString("sub"), groupId, postId, targetUid, post));
    }

    @Operation(summary = "Sent a message to a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Message successfully sent",
                    content = @Content)
    })
    @PostMapping("/targetUser/{targetUid}")
    public ResponseEntity sendDM(@AuthenticationPrincipal Jwt jwt, @PathVariable("targetUid") String targetUid, @RequestBody Post post) {

        return ResponseEntity.ok(postService.sendDM(jwt.getClaimAsString("sub"), targetUid, post));
    }

    @Operation(summary = "Created a post i target topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Message successfully sent",
                    content = @Content)
    })
    @PostMapping("/topic/{topicId}/post/{postId}/user/{targetUid}")
    public ResponseEntity answerToTopicPosts(@AuthenticationPrincipal Jwt jwt, @PathVariable("topicId") int topicId, @PathVariable("postId") int postId, @PathVariable("targetUid") String targetUid, @RequestBody Post post) {

        return ResponseEntity.ok(postService.answerToTopicPost(jwt.getClaimAsString("sub"), topicId, postId, targetUid, post));
    }

    @Operation(summary = "Updates a post")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppUser.class))})})
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Post post, @PathVariable int id) {

        if (post.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        postService.update(post);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Deletes a post by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )})})
    @DeleteMapping("/{id}")
    public ResponseEntity deletePostById(@PathVariable int id) {
        postService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
