package com.example.alumniportal.controller;

import com.example.alumniportal.dto.TopicDTO;
import com.example.alumniportal.mappers.TopicMapper;
import com.example.alumniportal.models.Topic;
import com.example.alumniportal.services.topic.TopicServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin("http://localhost:3000")
@AllArgsConstructor
@RequestMapping("api/v1/topic")
public class TopicController {

    private final TopicServiceImpl topicService;
    private final TopicMapper topicMapper;

//    private final PostServiceImpl postService;

    @Operation(summary = "Returns all topics")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TopicDTO.class)))})
    })
    @GetMapping("/get")
    public ResponseEntity getAllTopics() {
        Collection<TopicDTO> topicDTOS = topicMapper.topicToTopicDTO(topicService.findAll());
        return ResponseEntity.ok(topicDTOS);
    }


    @Operation(summary = "Returns a topic by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TopicDTO.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity getTopicById(@PathVariable Integer id) {
        TopicDTO topicDTO = topicMapper.topicToTopicDTO(topicService.findById(id));
        return ResponseEntity.ok(topicDTO);
    }

    @Operation(summary = "Updates a topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Topic.class))})})
    @PutMapping("/follow/{topicId}")
    public ResponseEntity joinTopic(@AuthenticationPrincipal Jwt jwt, @PathVariable("topicId") int topicId) {
        topicService.followTopic(jwt.getClaimAsString("sub"), topicId);

        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Creates a topic")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = @Content)
    })
    @PostMapping()
    public ResponseEntity addNewTopic(@AuthenticationPrincipal Jwt jwt, @RequestBody Topic topic) {
        Topic newTopic = topicService.addTopic(jwt.getClaimAsString("sub"), topic);
        URI location = URI.create("topic/" + newTopic.getId());
        return ResponseEntity.created(location).build();
    }
}
