package com.example.alumniportal.controller;

import com.example.alumniportal.ApiErrorResponse;
import com.example.alumniportal.dto.AppGroupDTO;
import com.example.alumniportal.mappers.AppGroupMapper;
import com.example.alumniportal.models.AppGroup;
import com.example.alumniportal.services.AppGroup.AppGroupServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@CrossOrigin("http://localhost:3000")
@AllArgsConstructor
@RequestMapping("api/v1/groups")
public class AppGroupController {

    private final AppGroupMapper appGroupMapper;

    private final AppGroupServiceImpl appGroupService;

    @Operation(summary = "Returns all characters groups")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = AppGroupDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Groups does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/get")
    public ResponseEntity getAllGroups() {
        Collection<AppGroupDTO> appGroupDTOListDTOS = appGroupMapper.appGroupToAppGroupDTO(appGroupService.findAll());

        return ResponseEntity.ok(appGroupDTOListDTOS);
    }

    @Operation(summary = "Gets a group by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppGroupDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Group does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity getGroupById(@PathVariable Integer id) {
        AppGroupDTO appGroupDTO = appGroupMapper.appGroupToAppGroupDTO(appGroupService.findById(id));
        return ResponseEntity.ok(appGroupDTO);
    }

    @Operation(summary = "Creates a new group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Group successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json")}),
            //schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Group not found with supplied ID",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity add(@AuthenticationPrincipal Jwt jwt, @RequestBody AppGroup appGroup) {
        AppGroup appGroup1 = appGroupService.addGroup(jwt.getClaimAsString("sub"), appGroup);
        URI location = URI.create("groups/" + appGroup1.getId());
        return ResponseEntity.created(location).build();
    }
}
