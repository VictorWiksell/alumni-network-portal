package com.example.alumniportal.controller;

import com.example.alumniportal.ApiErrorResponse;
import com.example.alumniportal.dto.AppUserDTO;
import com.example.alumniportal.mappers.AppUserMapper;
import com.example.alumniportal.models.AppUser;
import com.example.alumniportal.services.AppUser.AppUserServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@AllArgsConstructor
@RequestMapping("api/v1/users")
public class AppUserController {
    private final AppUserServiceImpl appUserServiceImpl;
    private final AppUserMapper appUserMapper;

    @Operation(summary = "Returns all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = AppUserDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "User does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/get")
    public ResponseEntity getAllUsers() {
        List<AppUserDTO> appUserDTOS = (List<AppUserDTO>) appUserMapper.appUserToAppUserDTO(appUserServiceImpl.findAll());
        return ResponseEntity.ok(appUserDTOS);
    }

    @Operation(summary = "Returns a user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppUserDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "User does not exist with supplied id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/user")
    public ResponseEntity findById(@AuthenticationPrincipal Jwt jwt) {
        AppUserDTO appUserDTO = appUserMapper.appUserToAppUserDTO(appUserServiceImpl.findById(jwt.getClaimAsString("sub")));
        return ResponseEntity.ok(appUserDTO);
    }

    @Operation(summary = "Updates a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "User successfully updated",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppUser.class))),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "User not found with supplied id",
                    content = @Content)
    })
    @PutMapping("/updateinfo")
    public ResponseEntity updateUser(@AuthenticationPrincipal Jwt jwt,
                                     @RequestBody AppUser appUser) {
        appUserServiceImpl.setInfo(appUser, jwt.getClaimAsString("sub"));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Returns the currently logged in user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppUser.class))}),
            @ApiResponse(responseCode = "404", description = "User does not exist with supplied id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})
    @GetMapping("current")
    public ResponseEntity getCurrentlyLoggedInUser(@AuthenticationPrincipal Jwt jwt) {
        return ResponseEntity.ok(
                appUserServiceImpl.findById(
                        jwt.getClaimAsString("sub")
                )
        );
    }

    @Operation(summary = "Registers a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "User successfully registered",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "User not found with supplied id",
                    content = @Content)
    })
    @PostMapping("register")
    public ResponseEntity addNewUserFromJwt(@AuthenticationPrincipal Jwt jwt) {
        String uid = jwt.getClaimAsString("sub");
        if (appUserServiceImpl.exists(uid))
            return ResponseEntity.badRequest().build();
        AppUser user = appUserServiceImpl.addUser(uid, jwt.getClaimAsString("name"));
        URI uri = URI.create("api/v1/users/" + user.getUid());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Updates a group by adding a user to group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AppUser.class))}),
            @ApiResponse(responseCode = "404", description = "Group does not exist with supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})})
    @PutMapping("/joingroup/{groupId}")
    public ResponseEntity joinGroup(@AuthenticationPrincipal Jwt jwt, @PathVariable("groupId") int groupId) {
        appUserServiceImpl.joinGroup(jwt.getClaimAsString("sub"), groupId);
        return ResponseEntity.noContent().build();
    }
}
