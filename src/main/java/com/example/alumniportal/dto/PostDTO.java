package com.example.alumniportal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

/***
 * A representation of Post entity as a DTO
 */
@Getter
@Setter
public class PostDTO {
    private Integer id;
    private String name;
    private String postMessage;
    private String picture;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", shape = JsonFormat.Shape.STRING)
    private String createDateTime;
    private String targetUser;
    private String senderUser;
    private int targetGroup;
    private int answeredTo;
    private int targetTopic;
}
