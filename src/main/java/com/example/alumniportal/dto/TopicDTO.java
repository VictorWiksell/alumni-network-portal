package com.example.alumniportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/***
 * A representation of Topic entity as a DTO
 */
@Setter
@Getter
public class TopicDTO {
    Integer id;
    String name;
    String description;
    Integer targetTopicIds;
    Set<String> appUsers;
    Set<Integer> topicPosts;
}
