package com.example.alumniportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/***
 * A representation of appGroup entity as a DTO
 */

@Getter
@Setter
public class AppGroupDTO {
    Integer id;
    String fullName;
    String description;
    boolean isPrivate;
    Set<String> appUser;
    Set<Integer> postIds;
}
