package com.example.alumniportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/***
 * A representation of appUser entity as a DTO
 */
@Getter
@Setter
public class AppUserDTO {
    String uid;
    String name;
    String picture;
    String status;
    String bio;
    String funFact;

    Set<Integer> appGroup;
    Set<Integer> topics;
    Set<Integer> sentPosts;
    Set<Integer> receivedPosts;

}
